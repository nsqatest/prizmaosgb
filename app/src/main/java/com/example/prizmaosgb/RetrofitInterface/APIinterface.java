package com.example.prizmaosgb.RetrofitInterface;

import com.example.prizmaosgb.Retrofit.Doc;
import com.example.prizmaosgb.Retrofit.Example;
import com.example.prizmaosgb.Retrofit.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIinterface {

    //    @POST("api/index2.php")
    //        @FormUrlEncoded
    //    Call<UserResponse> login(@Body UserInfo userInfo);


//
//    @Headers("Content-Type: application/json")
//    @GET("admin/den/{Id}")
//    Call<List<Doc>> docList(@Path("Id") String id);

    @Headers("Content-Type: application/json")
    @GET("admin/den/{Id}")
    Call<Example> exampleList(@Path("Id") String id);

//    @Headers("Content-Type: application/json")
//    @GET("admin/den/{Id}")
//    Call<List<User>> userlist(@Path("Id") String id);



}