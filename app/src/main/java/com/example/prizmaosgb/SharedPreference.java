package com.example.prizmaosgb;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {

    private static SharedPreference yourPreference;
    private SharedPreferences sharedPreferences;

    private SharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences("Token", Context.MODE_PRIVATE);
    }

    public static SharedPreference getInstance(Context context) {
        if (yourPreference == null) {
            yourPreference = new SharedPreference(context);
        }
        return yourPreference;
    }

    public void saveData(String key, String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public void clearData(String key) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.remove(key);
        prefsEditor.commit();
    }

    public String getData(String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, "");
        }
        return "";
    }
}

