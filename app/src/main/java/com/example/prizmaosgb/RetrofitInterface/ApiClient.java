package com.example.prizmaosgb.RetrofitInterface;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.prizmaosgb.RetrofitInterface.APIBaseUrl.BASE_URL;

public class ApiClient {

    private static Retrofit retrofit = null;
    //private static String Base_Url = "http://enessubas.site/";

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(new OkHttpClient())
                    .build();
            return retrofit;
        }
        return retrofit;

    }
}