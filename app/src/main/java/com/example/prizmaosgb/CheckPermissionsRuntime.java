package com.example.prizmaosgb;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class CheckPermissionsRuntime {

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1905;
    private Context ctx;

    public void Ctx(Context ctx) {
        this.ctx = ctx;
    }

    public boolean checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            int permissionINTERNET = ContextCompat.checkSelfPermission(ctx, android.Manifest.permission.INTERNET);
            int permissionACCESS_NETWORK_STATE = ContextCompat.checkSelfPermission(ctx, android.Manifest.permission.ACCESS_NETWORK_STATE);
            int permissionCAMERA = ContextCompat.checkSelfPermission(ctx, android.Manifest.permission.CAMERA);
            int permissionACCESS_WIFI_STATE = ContextCompat.checkSelfPermission(ctx, android.Manifest.permission.ACCESS_WIFI_STATE);
            List<String> listPermissionsNeeded = new ArrayList<>();
            if (permissionINTERNET != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.INTERNET);
            }
            if (permissionACCESS_NETWORK_STATE != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.ACCESS_NETWORK_STATE);
            }


            if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
            }


            if (permissionINTERNET != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.INTERNET);
            }

            if (permissionACCESS_WIFI_STATE != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_WIFI_STATE);
            }


            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions((Activity) ctx, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

}
