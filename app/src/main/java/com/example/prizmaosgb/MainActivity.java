package com.example.prizmaosgb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.prizmaosgb.Retrofit.Example;
import com.example.prizmaosgb.RetrofitInterface.APIinterface;
import com.example.prizmaosgb.RetrofitInterface.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    Button qrread;
    CheckPermissionsRuntime checkper = new CheckPermissionsRuntime();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkper.Ctx(MainActivity.this);

        if (!checkper.checkAndRequestPermissions()) {
            return;
        }
        qrread = (Button) findViewById(R.id.qrread);

        qrread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent qrReadActivity = new Intent(MainActivity.this, QRCodeActivity.class);
                    startActivityForResult(qrReadActivity, 1);
                } catch (Exception ex) {
                    Toast.makeText(getApplicationContext(), "İzinler Eksik", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 1 && resultCode == RESULT_OK) {

            String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
            String result = intent.getStringExtra("SCAN_RESULT");

            QrResult(result);
        }

    }

    public void QrResult(String result) {

        SharedPreference pre = SharedPreference.getInstance(getApplicationContext());
        String logintoken = pre.getData("logintoken");
        APIinterface request = ApiClient.getClient().create(APIinterface.class);

        Call<Example> call = request.exampleList(result);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                response.body();


                if (response.code() == 200) {
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("", "gecersiz");

            }

        });
    }


}


